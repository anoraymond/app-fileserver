var session = require('express-session');
var express = require('express');
var cookieParser = require("cookie-parser");

var app = express();

app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 120000 }
}));

app.get("/", (req, res) => {
    req.session.views = (req.session.views || 0) + 1;
    console.log("session id ", req.session.id);
    res.send(`number of views ${req.session.views}`);
})
app.set('trust proxy', 1); // trust first proxy



app.listen(3040, () => console.log("app is running..."));